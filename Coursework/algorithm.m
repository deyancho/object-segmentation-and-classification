function [subs, images_binary,normalized_images, back_n] =  algorithm(images_directory, extension, skip_data_gathering)
    
    if skip_data_gathering == 1 % if no data gathering do nothing
    else
        images =img_loader(images_directory, extension);   % images is 4-d array with rgb values, and 4th dim is the number of the image
        [m n k j] = size(images);
        back_n = extract_background(images,1); % extract the background of the image - could be either normalized or not
        normalized_images = normalize_images(images,1);  % normalize each of the images
        subs = uint8(zeros(m,n,k,j));  % create a 4d array for the difference between the normalized background and the normalized image
        for p = 1:j
            subs(:,:,:,p) = imabsdiff(normalized_images(:,:,:,p),back_n)*4;  % take the absolute difference between the normalized background and the normalized image - *4 because it's very dark
        end
        images_binary = plotter(subs,17,14,12); % plot the processed binary images.
        user_entry = input('Want to acquire training data (0,1)\n?'); 
        % 17 14 12
        image_processed_train = 1;
        imaged_processed_test = 1;
        while user_entry == 1
            user_train_pic = input('What image do you want to use for training(check the sequence of your images)? \n');
            [~,image_processed_train]=findobjects(images_binary(:,:,user_train_pic),uint8(images(:,:,:,user_train_pic)),image_processed_train,1);
            user_entry = input('Want to acquire more training data (0,1)\n?'); 

        end

        user_entry_test = input('Want to acquire testing data (0,1)\n?'); 

        while user_entry_test == 1
            user_train_pic = input('What image do you want to use for testing(check the sequence of your images)? \n');
            [~,imaged_processed_test]=findobjects(images_binary(:,:,user_train_pic),uint8(images(:,:,:,user_train_pic)),imaged_processed_test,0);
            user_entry_test = input('Want to acquire more testing data (0,1)\n?'); 
        end
    end
    
    
    %train_data = img_loader(strcat(pwd,'testingSet'))
    
    %doall(4);
    
    
    
    setDir = fullfile(pwd,'trainingSet');
    imds = imageDatastore(setDir,'IncludeSubfolders',true,'LabelSource',...
'foldernames');
    image_location = fileparts(imds.Files{1});
    trainingset = imageSet(strcat(image_location,'\..'),'recursive');
    bag = bagOfFeatures(trainingset);
    categoryClassifier = trainImageCategoryClassifier(trainingset,bag);
    setDir2 = fullfile(pwd,'testingSet');
    imds2 = imageDatastore(setDir2,'IncludeSubfolders',true,'LabelSource',...
'foldernames');
    image_location2 = fileparts(imds2.Files{1});
    testingset = imageSet(strcat(image_location2,'\..'),'recursive');
    confMatrix = evaluate(categoryClassifier,testingset)
%}
       
    
    
    