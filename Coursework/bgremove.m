  %test1 = myjpgload('f1.jpg',1);
  
  
  %img_norm = uint8(images(:,:,:,1));
  [m q y z] = size(norms)
  for i = 1:z
      partim_big = norms(:,:,:,i);
      for j = 1:3
      
          partim = partim_big(:,:,j)
          bgim = back_n(:,:,j);
          %partim = partim(:,:,1);
          %bgim = bgim(:,:,1);
          %partim = test1;

          %bgim =imread('lightgradgr.jpg');
          figure(4)
          colormap(gray)
          imshow(partim)

          % size of the image
          [H,W] = size(partim);
          outim = zeros(H,W);

          % gets bg
          for r = 1 : H
          for c = 1 : W
            outim(r,c) = double(partim(r,c))/double(bgim(r,c));
          end
          end

          % set up bin edges for histogram
          edges = zeros(256,1);
          for i = 1 : 256;
                  edges(i) = i-1;
          end

          figure(1)
          % image vector for the image
          imagevec = reshape(partim,1,H*W);      % turn image into long array
          myhist = histc(imagevec,edges)';        % do histogram
          plot(myhist)
          axis([0, 255, 0, 1.1*max(myhist)])

          figure(2)
          newedges = zeros(256,1);
          for i = 1 : 256;
                  newedges(i) = (i-1)/100;
          end
          oimagevec = (reshape(outim,1,H*W));      % turn image into long array
          omyhist = histc(oimagevec,newedges)';        % do histogram
          plot(omyhist)
          axis([0, max(100*oimagevec), 0, 1.1*max(omyhist)])

        %  ind = find(myhist==max(myhist))
         % try a threshold 4 on the to binary function for the 3 color channels on
         % the subs image

          figure()
          colormap(gray)
          binimage2 = ~im2bw(outim./double(bgim),0.95);       % do thresholding
          imshow(binimage2)
      end
  end