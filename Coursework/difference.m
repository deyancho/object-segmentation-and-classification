function substracted_no_abs = difference(image, background,show_abs)

    % This will return the absolute or non-absolute value of the difference
    % between 2 images with the same dimensions.
    image = uint8(image);
    if show_abs == 1
        substracted_no_abs = minus(image,background);
    else
        substracted_no_abs = minus(image,background);
    end
    figure();
    imshow(substracted_no_abs)