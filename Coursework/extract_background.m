function background_normalized = extract_background(images,show)
[dim1,dim2,dim3,dim4] = size(images);

background_not_norm = zeros(dim1,dim2,dim3);
background_normalized = zeros(dim1,dim2,dim3);

    for i = 1:dim1
        for j = 1:dim2
            reds = images(i,j,1,:) ;
            greens = images(i,j,2,:);
            blues = images(i,j,3,:);
            avgRed = median(reds);
            avgGreen = median(greens);
            avgBlue = median(blues);
            
            % Commented lines above if we want non-normalized background
            
            %background_not_norm(i,j,1) = avgRed;
            %background_not_norm(i,j,2) = avgGreen;
            %background_not_norm(i,j,3) = avgBlue;
            background_normalized(i,j,1) = 255*avgRed/(avgRed + avgGreen + avgBlue);
            background_normalized(i,j,2) = 255*avgGreen/(avgRed + avgGreen + avgBlue);
            background_normalized(i,j,3) = 255*avgBlue/(avgRed + avgGreen + avgBlue);
        end
    end

    %background_not_norm = uint8(background_not_norm);
    background_normalized = uint8(background_normalized);
    if show > 0
        %figure();
        %imshow(background_not_norm);
        figure();
        imshow(background_normalized);
    end
    end

