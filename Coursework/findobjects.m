function [correct, imageProcessed] = findobjects(image_bin, image_original, imageProcessed, train)
    
    % Some code taken and adapted from here: https://www.youtube.com/watch?v=XrC1r80-p-k
    figure()
    imshow(image_original);
    label= bwlabel(image_bin);
    correct = 1;
    vec = [];
    a=dir;  %  or  a=dir('folder')
    b=struct2cell(a);
    st = regionprops(label, 'BoundingBox' );
    
    if train==1
        disp('Labels 1-1 pound, 2 - 2 pounds, 3 - 50 pence, 4 - 20 pence, 5 - 5 pence, 6 - washer small hole')
        disp('7 - washer large hole, 8 - angle bracket, 9 - AAA battery, 10 - nut, 11 - false detection')
        if any(ismember(b(1,:),'trainingSet'))~=0               
        else
            mkdir('trainingSet');
        end

        cd 'trainingSet'
    elseif train == 0
        if any(ismember(b(1,:),'testingSet'))~=0               
        else
            mkdir('testingSet');
        end

        cd 'testingSet'
    end
    
    a=dir;  %  or  a=dir('folder')
    b=struct2cell(a);
    
    %{
    if any(ismember(b(1,:),'binaryObjects'))~=0               
    else
        mkdir('binaryObjects');
    end
    
    if any(ismember(b(1,:),'coloredObjects'))~=0               
    else
        mkdir('coloredObjects');
    end
    %}
   
    
    for j = 1:max(max(label));
        [row,col] = find(label==j);
        len=max(row)-min(row)+2;
        breadth = max(col)-min(col)+2;
        target=uint8(zeros([len breadth 3]));
        target_bin=logical((zeros([len breadth])));
        sy = min(col)-1;
        sx = min(row)-1;
        for i = 1:size(row,1)
            x=row(i,1)-sx;
            y=col(i,1)-sy;
            target(x,y,:)=image_original(row(i,1),col(i,1),:);
            target_bin(x,y)=image_bin(row(i,1),col(i,1));
            %target_bin(x,y,:) = image_b(row(i,1),col(i,1),:);
        end
       
    
     [target_len, target_width, dims] = size(target);
        
        
        
        if target_len > 35 & target_len && target_width > 35 & target_width   
            %path = 'trainingSet'
            
            thisBB = st(j).BoundingBox;
              rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],...
              'EdgeColor','r','LineWidth',2 )
            mytitle = strcat('Object Number: ' , num2str(correct));
            %imshow(target);
            
            title(mytitle);
            %imwrite(target, strcat(pwd,'\coloredObjects\',int2str(imageProcessed),'.jpg')) % change slash directions for unix
            %currentimage = rgb2gray(target);
            %imwrite(target_bin, strcat(pwd,'\binaryObjects\',int2str(imageProcessed),'.jpg')) % change slash directions for unix
            
            
            %vec(correct,:) = extractprops(target_bin,0,0,0,0,0);
            
            if train == 1
                trueclass= input(['Train image ',int2str(correct),' true class (1..11',')\n?']);
                
                if trueclass == 1
                        text(thisBB(1),thisBB(2),'1 pound')
                        if any(ismember(b(1,:),'OnePound'))~=0               
                        else
                            mkdir('OnePound');
                        end

                        imwrite(target, strcat(pwd,'\OnePound\',int2str(imageProcessed),'.jpg'))
                elseif trueclass == 2
                        text(thisBB(1),thisBB(2),'2 pounds')
                        if any(ismember(b(1,:),'TwoPounds'))~=0               
                        else
                            mkdir('TwoPounds');
                        end
                        imwrite(target, strcat(pwd,'\TwoPounds\',int2str(imageProcessed),'.jpg'));
                
                elseif trueclass == 3
                    text(thisBB(1),thisBB(2),'50 pence')
                    if any(ismember(b(1,:),'50Pence'))~=0               
                    else
                        mkdir('50Pence');
                    end
                    imwrite(target, strcat(pwd,'\50Pence\',int2str(imageProcessed),'.jpg'));
                elseif trueclass == 4
                    text(thisBB(1),thisBB(2),'20 pence')
                    if any(ismember(b(1,:),'20Pence'))~=0               
                    else
                        mkdir('20Pence');
                    end
                    imwrite(target, strcat(pwd,'\20Pence\',int2str(imageProcessed),'.jpg'));

                elseif trueclass == 5
                    text(thisBB(1),thisBB(2),'5 pence')
                    if any(ismember(b(1,:),'5Pence'))~=0               
                    else
                        mkdir('5Pence');
                    end
                    imwrite(target, strcat(pwd,'\5Pence\',int2str(imageProcessed),'.jpg'));
                elseif trueclass == 6
                    text(thisBB(1),thisBB(2),'Washer Small')
                    if any(ismember(b(1,:),'WasherSmallHole'))~=0               
                    else
                        mkdir('WasherSmallHole');
                    end
                    imwrite(target, strcat(pwd,'\WasherSmallHole\',int2str(imageProcessed),'.jpg'));    
                elseif trueclass == 7
                    text(thisBB(1),thisBB(2),'Washer Large')
                    if any(ismember(b(1,:),'WasherLargeHole'))~=0               
                    else
                        mkdir('WasherLargeHole');
                    end
                    imwrite(target, strcat(pwd,'\WasherLargeHole\',int2str(imageProcessed),'.jpg'));    

                elseif trueclass == 8
                    text(thisBB(1),thisBB(2),'Angle Bracket')
                    if any(ismember(b(1,:),'AngleBracket'))~=0               
                    else
                        mkdir('AngleBracket');
                    end
                    imwrite(target, strcat(pwd,'\AngleBracket\',int2str(imageProcessed),'.jpg'));   

                elseif trueclass == 9
                    text(thisBB(1),thisBB(2),'battery')
                    if any(ismember(b(1,:),'AAABatery'))~=0               
                    else
                        mkdir('AAABatery');
                    end
                    imwrite(target, strcat(pwd,'\AAABatery\',int2str(imageProcessed),'.jpg'));

                elseif trueclass == 10
                    text(thisBB(1),thisBB(2),'nut')
                    if any(ismember(b(1,:),'nut'))~=0               
                    else
                        mkdir('nut');
                    end
                    imwrite(target, strcat(pwd,'\nut\',int2str(imageProcessed),'.jpg')); 

                elseif trueclass == 11
                    text(thisBB(1),thisBB(2),'blob')
                    if any(ismember(b(1,:),'FalseDetection'))~=0               
                    else
                        mkdir('FalseDetection');
                    end
                    imwrite(target, strcat(pwd,'\FalseDetection\',int2str(imageProcessed),'.jpg')); 
                
                end
                
            elseif train==0
                trueclass= input(['Test image ',int2str(correct),' true class (1..11',')\n?']);
                if trueclass == 1
                        text(thisBB(1),thisBB(2),'1 pound')
                        if any(ismember(b(1,:),'OnePound'))~=0               
                        else
                            mkdir('OnePound');
                        end

                        imwrite(target, strcat(pwd,'\OnePound\',int2str(imageProcessed),'.jpg'));
                elseif trueclass == 2
                        text(thisBB(1),thisBB(2),'2 pounds')
                        if any(ismember(b(1,:),'TwoPounds'))~=0               
                        else
                            mkdir('TwoPounds');
                        end
                        imwrite(target, strcat(pwd,'\TwoPounds\',int2str(imageProcessed),'.jpg'));

                elseif trueclass == 3
                    text(thisBB(1),thisBB(2),'50 pence')
                    if any(ismember(b(1,:),'50Pence'))~=0               
                    else
                        mkdir('50Pence');
                    end
                    imwrite(target, strcat(pwd,'\50Pence\',int2str(imageProcessed),'.jpg'));
                elseif trueclass == 4
                    text(thisBB(1),thisBB(2),'20 pence')
                    if any(ismember(b(1,:),'20Pence'))~=0               
                    else
                        mkdir('20Pence');
                    end
                    imwrite(target, strcat(pwd,'\20Pence\',int2str(imageProcessed),'.jpg'));

                elseif trueclass == 5
                    text(thisBB(1),thisBB(2),'5 pence')
                    if any(ismember(b(1,:),'5Pence'))~=0               
                    else
                        mkdir('5Pence');
                    end
                    imwrite(target, strcat(pwd,'\5Pence\',int2str(imageProcessed),'.jpg'));
                elseif trueclass == 6
                    text(thisBB(1),thisBB(2),'Washer Small')
                    if any(ismember(b(1,:),'WasherSmallHole'))~=0               
                    else
                        mkdir('WasherSmallHole');
                    end
                    imwrite(target, strcat(pwd,'\WasherSmallHole\',int2str(imageProcessed),'.jpg'))    
                elseif trueclass == 7
                    text(thisBB(1),thisBB(2),'Washer Large')
                    if any(ismember(b(1,:),'WasherLargeHole'))~=0               
                    else
                        mkdir('WasherLargeHole');
                    end
                    imwrite(target, strcat(pwd,'\WasherLargeHole\',int2str(imageProcessed),'.jpg'))    

                elseif trueclass == 8
                    text(thisBB(1),thisBB(2),'bracket')
                    if any(ismember(b(1,:),'AngleBracket'))~=0               
                    else
                        mkdir('AngleBracket');
                    end
                    imwrite(target, strcat(pwd,'\AngleBracket\',int2str(imageProcessed),'.jpg'))   

                elseif trueclass == 9
                    text(thisBB(1),thisBB(2),'battery')
                    if any(ismember(b(1,:),'AAABatery'))~=0               
                    else
                        mkdir('AAABatery');
                    end
                    imwrite(target, strcat(pwd,'\AAABatery\',int2str(imageProcessed),'.jpg'))

                elseif trueclass == 10
                    text(thisBB(1),thisBB(2),'nut')
                    if any(ismember(b(1,:),'nut'))~=0               
                    else
                        mkdir('nut');
                    end
                    imwrite(target, strcat(pwd,'\nut\',int2str(imageProcessed),'.jpg'))   

                elseif trueclass == 11
                    text(thisBB(1),thisBB(2),'blob')
                    if any(ismember(b(1,:),'FalseDetection'))~=0               
                    else
                        mkdir('FalseDetection');
                    end
                    imwrite(target, strcat(pwd,'\FalseDetection\',int2str(imageProcessed),'.jpg'))   
                
            else
                imwrite(target, strcat(pwd,'\',int2str(imageProcessed),'.jpg'));
            end
            
            end
        correct = correct+1;
        imageProcessed = imageProcessed + 1;
        end
    end
    cd ..;
    
    