
function X = img_loader(Folder,ImgType)
    Imgs = dir([Folder '/' ImgType]);
    NumImgs = size(Imgs,1);
    image = uint8(imread([Folder '/' Imgs(1).name]));
    [m n k] = size(image);

    X = zeros([m n k NumImgs]);
    for i=1:NumImgs

      %figure()
      image = (imread([Folder '/' Imgs(i).name]));
      X(:,:,:,i) = image;
      %imshow(uint8(X(:,:,:,i)))

    end
    