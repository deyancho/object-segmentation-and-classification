function normalized_images = normalize_images(images,show)
    [m n k d] = size(images);
    normalized_images = uint8(zeros(m,n,k,d));

    for i = 1:d
        normalized_images(:,:,:,i) = normalize_img(uint8(images(:,:,:,i)),0);
        if show > 0
            %figure()
            %imshow(normalized_images(:,:,:,i))
        end
    end