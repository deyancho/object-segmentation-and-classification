function normalized_img = normalize_img(rgb_image,show)
    [r,c,ch] = size(rgb_image);
    normalized_img = zeros(r,c,ch);
    rgb_image = im2double(rgb_image);
    
    for i = 1:ch
        for j = 1:r
            for m = 1:c
                normalized_img(j,m,i) = rgb_image(j,m,i)/(rgb_image(j,m,1)+rgb_image(j,m,2)+rgb_image(j,m,3));
            end
        end
    end
    
    normalized_img = uint8(normalized_img*255);
    
    if show == 1
        figure();
        imshow(normalized_img);
    end