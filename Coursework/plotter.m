function images_binary = plotter(subs_i,threshr,threshg,threshb)
    [m,n,k,d] = size(subs_i);
    images_binary = zeros(m,n,d);
    for i = 1:d
        % bwmorph perform morphological operation on the binary image - https://www.youtube.com/watch?v=OP1ngw3apLg
        im_i_r = to_binary(subs_i(:,:,1,i),threshr);   % get the binarized image value for the substracted image for the red channel
        im_i_r = bwmorph(im_i_r,'erode');
        im_i_g = to_binary(subs_i(:,:,2,i),threshg);   % get the binarized image value for the substracted image for the green channel
        im_i_g = bwmorph(im_i_g,'erode');
        im_i_b = to_binary(subs_i(:,:,3,i),threshb);   % get the binarized image value for the substracted image for the blue channel
        im_i_b = bwmorph(im_i_b,'erode');
        subs_i_bin = im_i_r & im_i_g & im_i_b;  % AND the 3 images. 
        subs_i_bin = ~subs_i_bin;       % flip the white and green bits
        subs_i_bin = bwmorph(subs_i_bin,'erode');
        images_binary(:,:,i) = subs_i_bin;
        figure()
        imshow(images_binary(:,:,i))
        
    end