function images_binary = plotter_bin(images)

    [m,n,k,d] = size(images);
    images_binary = zeros(m,n,d);
    for i = 1:d

        [histr, histg, histb] = three_channel_hist(images(:,:,:,i));
        threshr = findthresh(histr,1000,0);
        threshg = findthresh(histg,1000,0);
        threshb = findthresh(histb,1000,0);
        
        im_i_r = to_binary(images(:,:,1,i),threshr);
        
        im_i_g = to_binary(images(:,:,2,i),threshg);

        im_i_b = to_binary(images(:,:,3,i),threshb);

        subs_i_bin = im_i_r & im_i_g & im_i_b;
        subs_i_bin = ~subs_i_bin;
        
        images_binary(:,:,i) = subs_i_bin;

    end