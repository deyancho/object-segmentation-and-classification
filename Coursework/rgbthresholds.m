function [r_thresh, g_thresh, b_thresh] = rgbthresholds(red_hist, param_red,show_red, green_hist, param_green,show_green, blue_hist, param_blue,show_blue)
    r_thresh = findthresh(red_hist,param_red, show_red);
    g_thresh = findthresh(green_hist, param_green, show_green);
    b_thresh = findthresh(blue_hist, param_blue, show_blue);
    