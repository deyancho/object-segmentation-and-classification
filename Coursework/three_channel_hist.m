function [red_hist, green_hist, blue_hist] = three_channel_hist(substractedImg,show)
    red_dim = substractedImg(:,:,1);
    green_dim = substractedImg(:,:,2);
    blue_dim = substractedImg(:,:,3);
    
    [red_hist, edges_red] = dohist(red_dim,show);    
    [green_hist, edges_green] = dohist(green_dim,show);        
    [blue_hist, edges_blue] = dohist(blue_dim,show);
