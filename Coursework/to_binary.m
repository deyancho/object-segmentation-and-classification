function binary_img = to_binary(image, threshold)
    [R,C] = size(image);
    binary_img = zeros(R,C);
    for row = 1:R
        for col = 1:C
            if image(row, col) < threshold
                binary_img(row,col) = 1;
            else
                binary_img(row, col) = 0;
            end
        end
    end